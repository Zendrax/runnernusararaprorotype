﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class RunnerUIMnager : MonoBehaviour
{
    [SerializeField] Image StaminaBar;
    [SerializeField] TextMeshProUGUI scoreUI;

    public void updateUIStamina(float f)
    {
        StaminaBar.fillAmount = f / 100;
    }
    public void updateUIScore(float f)
    {
        scoreUI.text = f.ToString("F0");
    }

}
