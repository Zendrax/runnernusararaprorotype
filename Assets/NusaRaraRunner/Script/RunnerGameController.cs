﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RunnerGameController : MonoBehaviour
{

    [SerializeField] Transform PlayerStartPosition;
    [SerializeField] GameObject Player;
    public bool gameStart;
    public bool gameOver;
    public float miniGameScore;

    public void StartGame()
    {
        if (gameOver)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        else
        {
            gameStart = true;
        }


    }
    // Start is called before the first frame update
    void Start()
    {
        initializePlayer();
    }
    void initializePlayer()
    {
        GameObject temp = Instantiate(Player);
        temp.transform.position = PlayerStartPosition.transform.position;
        temp.SetActive(true);
    }

    public void endGame()
    {
        gameStart = false;
        gameOver = true;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
