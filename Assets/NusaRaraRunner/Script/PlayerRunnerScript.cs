﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRunnerScript : MonoBehaviour
{
    [SerializeField] float gravity = 20.0f;
    [SerializeField] float jumpHeight = 2.5f;
    [SerializeField] bool grounded = false;
    [SerializeField] float playerStamina;
    [SerializeField] float moveSpeed;
    [SerializeField] float acceleroMultiplier;
    [SerializeField] Rigidbody r;
    [SerializeField] RunnerGameController RGC;
    [SerializeField] RunnerUIMnager RUM;

    // Start is called before the first frame update
    void Start()
    {
        r = GetComponent<Rigidbody>();
        RGC = GameObject.FindObjectOfType<RunnerGameController>();
        RUM = GameObject.FindObjectOfType<RunnerUIMnager>();
        playerStamina = 100f;
    }

    // Update is called once per frame
    void Update()
    {
        if (!RGC.gameStart)
        {
            return;
        }

        playerStamina -= Time.deltaTime;
        RUM.updateUIStamina(playerStamina);
        if (Input.GetMouseButtonDown(0))
        {
            r.velocity = new Vector3(r.velocity.x, CalculateJumpVerticalSpeed(), r.velocity.z);
        }

        playerMovement();

#if !UNITY_ANDROID

        inputAccelero();

#endif


    }

    private void inputAccelero()
    {
        float dirx = Input.acceleration.x;
        Vector3 position = this.transform.position;
        position.x += moveSpeed * dirx * Time.deltaTime * acceleroMultiplier;
        position.x = Mathf.Clamp(position.x, -2.2f, 2.2f);
        this.transform.position = position;
    }

    void playerMovement()
    {
        Vector3 position = this.transform.position;
        if (Input.GetKey(KeyCode.A))
        {
            position.x -= moveSpeed * Time.deltaTime;
            //this.transform.position = position;
        }
        if (Input.GetKey(KeyCode.D))
        {
            position.x += moveSpeed * Time.deltaTime;
            //this.transform.position = position;
        }
        position.x = Mathf.Clamp(position.x, -2.2f, 2.2f);
        this.transform.position = position;

    }

    private void FixedUpdate()
    {
       
        r.AddForce(new Vector3(0, -gravity * r.mass, 0));

        grounded = false;
    }
    float CalculateJumpVerticalSpeed()
    {
        return Mathf.Sqrt(2 * jumpHeight * gravity);
    }
    void OnCollisionStay()
    {
        grounded = true;
    }


    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Obstacle"))
        {
            playergotDamage(8f);
            other.gameObject.SetActive(false);
        }
    }

    private void playergotDamage(float v)
    {
        playerStamina -= v;

    }
}
