﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour
{
    [SerializeField] Camera mainCamera;
    [SerializeField] Transform startPosition;
    [SerializeField] PlatformTile tilePrefab;
    [SerializeField] float movingSpeed = 10;
    [SerializeField] int tileToPreSpawn = 10;
    [SerializeField] int tileWithoutObstacle = 3;//buat awal2 user pemanasan main

    [SerializeField] List<PlatformTile> listSpawnedTiles = new List<PlatformTile>();
    [SerializeField] int nextTiletoActivate = -1;
    public bool gameOver;

    [SerializeField] RunnerUIMnager RUM;
    [SerializeField] RunnerGameController RGC;
    
    void Start()
    {
        RUM = GameObject.FindObjectOfType<RunnerUIMnager>();
        RGC = GameObject.FindObjectOfType<RunnerGameController>();

        initializePlatform();
       
    }

    void Update()
    {
        PlatformMovement();
        PlatformLooping();
    }

    void PlatformMovement()
    {
        if (!RGC.gameOver && RGC.gameStart)
        {
            transform.Translate(-listSpawnedTiles[0].transform.forward * Time.deltaTime * (movingSpeed + (RGC.miniGameScore / 500)), Space.World);
            RGC.miniGameScore += Time.deltaTime * movingSpeed / 2;
            RUM.updateUIScore(RGC.miniGameScore);
        }
    }

    void PlatformLooping()
    {
        if (mainCamera.WorldToViewportPoint(listSpawnedTiles[0].endPoint.position).z < 0)
        {
            PlatformTile tileTmp = listSpawnedTiles[0];
            listSpawnedTiles.RemoveAt(0);
            tileTmp.transform.position = listSpawnedTiles[listSpawnedTiles.Count - 1].endPoint.position - tileTmp.startPoint.localPosition;
            int temp = 0;
            if (RGC.miniGameScore <= 50)
            {
                temp = 3;
            }
            else if (RGC.miniGameScore <= 150)
            {
                temp = 2;
            }
            else
            {
                temp = 1;
            }
            tileTmp.activateRandomObstacle(temp);
            listSpawnedTiles.Add(tileTmp);

        }
    }

    void initializePlatform()
    {
        Vector3 spawnPosition = startPosition.position;
        int tilesWithoutObstacleTmp = tileWithoutObstacle;
        for (int i = 0; i < tileToPreSpawn; i++)
        {
            spawnPosition -= tilePrefab.startPoint.localPosition;
            PlatformTile spawnedTile = Instantiate(tilePrefab, spawnPosition, Quaternion.identity) as PlatformTile;
            if (tilesWithoutObstacleTmp > 0)
            {
                spawnedTile.deactivateAllObstacle();
                tilesWithoutObstacleTmp--;
            }
            else
            {
                spawnedTile.activateRandomObstacle(3);
            }
            spawnPosition = spawnedTile.endPoint.position;
            spawnedTile.transform.SetParent(transform);
            listSpawnedTiles.Add(spawnedTile);
        }
        RGC.miniGameScore = 0;
    }
}
