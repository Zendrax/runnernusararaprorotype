﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformTile : MonoBehaviour
{

    public Transform startPoint, endPoint;
    [SerializeField] GameObject[] allObstacles;//tempat taro obstacle yg bakal di random
    [SerializeField] GameObject[] allBonusSpawnPoint;
    [SerializeField] GameObject[] allBonus;

    public void deactivateAllObstacle()
    {
        foreach(GameObject go in allObstacles)
        {
            go.SetActive(false);
        }
    }

    public void activateRandomObstacle(int level)
    { 
        deactivateAllObstacle();    
        int randomNumber = Random.Range(0, allObstacles.Length/level);
        allObstacles[randomNumber].SetActive(true);
        for(int i = 0; i < allObstacles[randomNumber].transform.childCount;i++)
        {
            allObstacles[randomNumber].transform.GetChild(i).gameObject.SetActive(true);
        }
        activatebuff();
    }


    void deactivateAllBuff()
    {
        foreach(GameObject go in allBonus)
        {
            go.SetActive(false);
        }
    }

    void activatebuff()
    {
        deactivateAllBuff();
        int jumlahItem = Random.Range(0, allBonus.Length);
        List<GameObject> templistspawnPoint = new List<GameObject>();
        List<GameObject> templistbuff = new List<GameObject>();
        foreach (GameObject go in allBonusSpawnPoint)
        {
            templistspawnPoint.Add(go);
        }      
        foreach (GameObject go in allBonus)
        {
            templistbuff.Add(go);
        }

        for (int i = 0; i < jumlahItem; i++)
        {
            int selectedBuff = Random.Range(0, templistbuff.Count);
            int selectedSpawnPoint = Random.Range(0, templistspawnPoint.Count);
            templistbuff[selectedBuff].transform.position = templistspawnPoint[selectedSpawnPoint].transform.position;
            templistbuff[selectedBuff].SetActive(true);
            templistbuff.RemoveAt(selectedBuff);
            templistspawnPoint.RemoveAt(selectedSpawnPoint);
        }

    }
  
}
